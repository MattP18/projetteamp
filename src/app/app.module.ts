import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './components/app/app.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateDrawingComponent } from './create-drawing/create-drawing.component';
import { SizeDrawing } from './services/createDrawing.service';
//import { MatDialog} from '@angular/material/dialog';
//import { OverlayModule } from '@angular/cdk/overlay';

@NgModule({
    declarations: [AppComponent, CreateDrawingComponent],
    imports: [BrowserModule, HttpClientModule, MatButtonModule, MatInputModule, BrowserAnimationsModule],
    providers: [SizeDrawing],
    bootstrap: [AppComponent],
})
export class AppModule {}
