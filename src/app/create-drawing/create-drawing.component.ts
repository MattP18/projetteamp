import { Component} from '@angular/core';
import { SizeDrawing } from '../services/createDrawing.service';
import { HostListener } from "@angular/core";
//import {MatDialogModule} from '@angular/material/dialog';


@Component({
  selector: 'app-create-drawing',
  templateUrl: './create-drawing.component.html',
  styleUrls: ['./create-drawing.component.scss']
})

export class CreateDrawingComponent{
  width:number = window.innerWidth;
  height:number = window.innerHeight;
  modified:boolean = false;

  constructor(size:SizeDrawing) { 
    this.width = window.innerWidth;
    this.height = window.innerHeight;
  }

  setWidth(width: number){
    this.width = width;
  }

  setHeight(height: number){
    this.height = height;
  }

  @HostListener('window:resize', ['$event'])
    getScreenSize(event?: any) {
      if(this.modified == false){
          this.setHeight(window.innerHeight);
          this.setWidth(window.innerWidth);
      }
    }

  modifiedSize(){
    this.modified = true;
    /*let width:number = document.getElementById("width")?.innerText;
    if(width != null && width > '0'){
        this.setWidth(width)
    }
    */ // Cancer bad cette shit
  }

  returnHandler(){
    document.location.href = ''; // LINK URL DE PAGE D'ACCEUIL***
  }

  drawHandler(){
    document.location.href = ''; // LINK URL DE CREATION DE DESSIN***
  }

 

}
