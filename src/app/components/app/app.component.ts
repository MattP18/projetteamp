import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Message } from '../../../../../common/communication/message';
import { IndexService } from '../../services/index/index.service';
import {MatDialog} from '@angular/material/dialog';
import { CreateDrawingComponent } from 'src/app/create-drawing/create-drawing.component';




@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    readonly title: string = 'LOG3900';
    message = new BehaviorSubject<string>('');

    constructor(private basicService: IndexService, private dialog: MatDialog) {
        
        this.basicService
            .basicGet()
            .pipe(map((message: Message) => `${message.title} ${message.body}`))
            .subscribe(this.message);
    }

    //mat-raised-button onclick="window.location.href='../create-drawing/create-drawing.component.html';"
    createForm(){
        this.dialog.open(CreateDrawingComponent);
    }
}
